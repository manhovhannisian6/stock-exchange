from datetime import datetime
import numpy as np
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, DateFormatter, MonthLocator
import pandas as pd


TIMESTAMP_STD = 2 * 10 ** 9
   

def print_loss(predict, x_test, y_test, x_train, y_train, loss_f=mean_absolute_error):
    test_loss = loss_f( y_test, predict(x_test) ).item()
    train_loss = loss_f( y_train, predict(x_train) ).item()
    full_loss = ( train_loss * len(y_train) + test_loss * len(y_test) ) / ( len(y_train) + len(y_test) )

    print('\nTrain loss =', train_loss)
    print('Full loss =', full_loss)
    print('Test loss =', test_loss, '\n')

    return train_loss, test_loss


def get_dates_axis(dates):
    """
    Chages format of dates
    """
    def change_format(date):
        return datetime.strptime(date, '%Y-%m-%d')

    return [change_format(date) for date in dates]


def graphics(y, y_pred, dates, x_count=20, title='', save=False, path=None):
    """
    Shows graph according to y and y_pred
    and saves as picture if needed
    """
    x = get_dates_axis(dates[-x_count:])

    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    ax.plot(x, y[-x_count:], label='Արժեքը')
    ax.plot(x, y_pred[-x_count:], label='Գուշակված արժեքը')
    if len(x) <= 31:
        ax.xaxis.set_major_locator(DayLocator(interval=3))
    elif len(x) <= 500:
        ax.xaxis.set_major_locator(MonthLocator(interval=1))
    else:
        ax.xaxis.set_major_locator(MonthLocator(interval=2))
    ax.xaxis.set_major_formatter(DateFormatter('%d %b %Y'))
    ax.set_title(title)

    fig.autofmt_xdate()

    ax.grid()
    ax.legend()

    if save:
        plt.savefig(path)
    plt.show()


def get_names():
    """
    Returns results from results.csv file.
    """
    pd_data = pd.read_csv('./results.csv')

    data = []

    for i in range(len(pd_data)):
        row = pd_data.iloc[i]
        data.append(row['symbol'])

    return data


def date_to_timestamp(date):
    if isinstance(date, str):
        date = datetime.strptime(date, '%Y-%m-%d')
    
    return datetime.timestamp(date) / TIMESTAMP_STD
